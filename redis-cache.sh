#!/bin/bash
# Colors
CSI="\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"

# Check root access
if [[ "$EUID" -ne 0 ]]; then
	echo -e "${CRED}Sorry, you need to run this as root${CEND}"
	exit 1
fi

echo 'deb http://packages.dotdeb.org jessie all' > /etc/apt/sources.list.d/dotdeb.list
wget https://www.dotdeb.org/dotdeb.gpg
apt-key add dotdeb.gpg &>/dev/null
apt-get update && apt-get upgrade -y &>/dev/null
apt-get install redis-server -y &>/dev/null
service redis-server start

# We're done !
echo ""
echo -e "       ${CGREEN}installation successful !${CEND}"
echo ""