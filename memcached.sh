#!/bin/bash
apt-get update && apt-get install gcc make autoconf libc-dev pkg-config  plesk-php70-dev zlib1g-dev libmemcached-dev unzip -y &>/dev/null
cd /opt/plesk/php/7.0/include/php/ext
wget -O phpmemcached-php7.zip https://github.com/php-memcached-dev/php-memcached/archive/php7.zip
unzip phpmemcached-php7.zip
cd php-memcached-php7/
/opt/plesk/php/7.0/bin/phpize
./configure --with-php-config=/opt/plesk/php/7.0/bin/php-config
export CFLAGS="-march=native -O2 -fomit-frame-pointer -pipe"
make
make install
ls -la /opt/plesk/php/7.0/lib/php/modules/
echo "extension=memcached.so" >/opt/plesk/php/7.0/etc/php.d/memcached.ini
/usr/local/psa/bin/php_handler --reread
service plesk-php70-fpm restart
apt-get install memcached
service memcached start