### Plesk Onyx Scripts collection

Some script to install / update / improve Plesk Onyx configuration. Tested on Ubuntu 16.04 LTS by default.
Simply run the command to install a script


**Script to compile Nginx from source with TLS 1.3 Support + Brotli**


-----

others modules included :
* ngx_cache_purge
* memc-nginx-module
* headers-more-nginx-module
* ngx_devel_kit
* echo-nginx-module
* redis2-nginx-module
* ngx_http_redis-0.3.8
* srcache-nginx-modul
* set-misc-nginx-module
* ngx_coolkit
* ngx_slowfs_cache
* ngx_http_substitutions_filter_module
* nginxdynamic_tls_records_1.11.5

To run the script
```
bash <(wget --no-check-certificate -O - https://git.virtubox.net/virtubox/plesk-script/src/master/nginx/build.sh)
```


