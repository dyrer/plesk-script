#!/bin/bash
apt-get update && apt-get install gcc make autoconf libc-dev pkg-config  plesk-php70-dev unzip -y &>/dev/null
cd /opt/plesk/php/7.0/include/php/ext/
wget https://github.com/Zakay/geoip/archive/master.zip
unzip master.zip
cd geoip-master
/opt/plesk/php/7.0/bin/phpize
./configure --with-php-config=/opt/plesk/php/7.0/bin/php-config
make
make install
echo "extension = geoip.so"> /opt/plesk/php/7.0/etc/php.d/geoip.ini
plesk bin php_handler --reread
service plesk-php70-fpm restart